"""
LeetCode
Title : 118. Pascal's Triangle
URL : https://leetcode.com/problems/pascals-triangle/
Difficulty : Easy
"""

def generate(numRows):
    result = [[1], [1,1]]
    if numRows < 2:
        return result[:numRows]

    index = 2
    while index < numRows:
        add_list = [None] * (index+1)
        add_list[0] = add_list[-1] = 1
        for i, j in enumerate(add_list[1:-1]):
            add_list[i+1] = result[index-1][i] + result[index-1][i+1]
        result.append(add_list)
        index += 1
    return result
