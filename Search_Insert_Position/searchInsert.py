"""
LeetCode
Title : 35. Search Insert Position
URL : https://leetcode.com/problems/search-insert-position/
Difficulty : easy
"""
def searchInsert(nums:list, target:int) -> int:
    for index, _element in enumerate(nums):
        if target < _element:
            return index

searchInsert([1,2,3,4], 0)