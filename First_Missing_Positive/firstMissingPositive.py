"""
LeetCode
Title : 41. First Missing Positive
URL : https://leetcode.com/problems/first-missing-positive/
Difficulty : Hard
"""

nums = [1,2,0]
def firstMissingPositive(nums):
    _dict = {}
    for i in nums:
        if i > 0:
            _dict[i] = False
    _count = 1

    while True:
        if _dict.get(_count, True):
            return _count
        _count += 1   
firstMissingPositive(nums)