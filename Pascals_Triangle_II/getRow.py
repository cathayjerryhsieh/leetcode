"""
LeetCode
Title : 119. Pascal's Triangle II
URL : https://leetcode.com/problems/pascals-triangle-ii/
Difficulty : Easy
"""

def getRow(rowIndex):
    result = [[1], [1,1]]
    if rowIndex < 2:
        return result[rowIndex]

    index = 2
    while index < rowIndex+1:
        add_list = [None] * (index+1)
        add_list[0] = add_list[-1] = 1
        for i, j in enumerate(add_list[1:-1]):
            add_list[i+1] = result[index-1][i] + result[index-1][i+1]
        result.append(add_list)
        index += 1
    return result[-1]