"""
LeetCode
Title : 57. Insert Interval
URL : https://leetcode.com/problems/insert-interval/
Difficulty : Medium
"""

def insert(intervals: list, newInterval: list) -> list:
    if not intervals:
        return [newInterval]
    result = [] 
    mixed_list = []
    index = 0
    for i, j in intervals:
        if j < newInterval[0]:
            index += 1
            result.append([i,j])
            continue
        elif i > newInterval[1]:
            result.append([i,j])
            continue
        else:
            mixed_list.extend([i,j])
    if mixed_list:
        mixed_list = [mixed_list[0],mixed_list[-1]]
        if mixed_list[0] > newInterval[0]:
            mixed_list[0] = newInterval[0]
        if mixed_list[1] < newInterval[1]:
            mixed_list[1] = newInterval[1]
        result.insert(index, mixed_list)
    elif index == 0 and not mixed_list:
        result.insert(index, newInterval)
    elif index > 0 and not mixed_list:
        result.insert(index, newInterval)
    return result

intervels = [[1,2],[3,5],[6,7],[8,10],[12,16]]
newintervel = [4,8]

insert(intervels, newintervel)