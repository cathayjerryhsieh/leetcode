"""
LeetCode
Title : 75. Sort Colors
URL : https://leetcode.com/problems/sort-colors/
Difficulty : Medium
"""

def sortColors(nums: list) -> list:
    last = len(nums)-1
    start = 0
    index = 0
    while last >= index:
        if nums[index] == 0:
            nums[start], nums[index] = nums[index], nums[start]
            start += 1
        elif nums[index] == 2:
            nums[last], nums[index] = nums[index], nums[last]
            last -= 1
            index -= 1
        index += 1
    return nums

sortColors([2,1,0])
