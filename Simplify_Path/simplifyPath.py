"""
LeetCode
Title : 71. Simplify Path
URL : https://leetcode.com/problems/simplify-path/
Difficulty : Medium
"""

import re

def simplifyPath(path):
    list_by_split = re.findall(r'[^/]+', path)
    result = []
    for _path in list_by_split:
        if _path == '..':
            if result:
                result.pop()
        elif _path != '.':
            result.append(_path)
    return '/'+'/'.join(result)
