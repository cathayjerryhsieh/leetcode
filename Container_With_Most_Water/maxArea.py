"""
LeetCode
Title : 11. Container With Most Water
URL : https://leetcode.com/problems/container-with-most-water/
Difficulty : Medium
"""

def maxArea(height: list) -> int:
    heightest = 0
    for index, _water in enumerate(height):
        weight = index
        water_level = 0
        while index > 0:
            index -= 1
            if water_level >= _water:
                break
            elif water_level < height[index]:
                water_level = height[index]
        weight -= index
        if water_level >= _water:
            water_level = _water
        elif water_level <= _water:
            _water = water_level
        ele_water_level = weight*_water
        if heightest < ele_water_level:
            heightest = ele_water_level
    return heightest

print(maxArea([4,3,2,9,4]))
