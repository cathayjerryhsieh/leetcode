"""
LeetCode
Title : 38. Count and Say
URL : https://leetcode.com/problems/count-and-say/
Difficulty : Easy
"""

def countAndSay(n: int) -> str:
    stack_num = "1"
    if n == 1:
        return stack_num
    for i in range(n-1):
        last_num = stack_num[0]
        stack_num_2 = ""
        count = 0
        for j in stack_num:
            if j == last_num:
                count += 1
            else:
                stack_num_2 += str(count) + str(last_num)
                count = 1
                last_num = j
        stack_num = stack_num_2 + str(count) + str(last_num)
    return stack_num

print(countAndSay(10))