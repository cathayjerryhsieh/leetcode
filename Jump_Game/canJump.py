"""
LeetCode
Title : 55. Jump Game
URL : https://leetcode.com/problems/jump-game/
Difficulty : Medium
"""

# my first idea
def canJump(nums: list) -> bool:
    list_lenght = len(nums)
    if list_lenght <= 1:
        return True
    _list = [nums[0]]
    vaild = False
    current = 0
    while _list:
        index = _list.pop()
        if index+1 >= list_lenght:
            return True
        if index == 0:
            return False
        for i in range(current+1,index+1,1):
            if nums[i]+i >= list_lenght:
                return True
            _list.append(nums[i]+i)
        current = index

    return vaild

# best solution
def canJump(nums: list) -> bool:
    last_change = len(nums)-1
    for i in range(last_change, -1, -1):
        if nums[i]+i >= last_change:
            last_change = i
    return last_change == 0
