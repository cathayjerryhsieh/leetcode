"""
LeetCode
Title : 50. Pow(x, n)
URL : https://leetcode.com/problems/powx-n/
Difficulty : Medium
"""

def myPow( x: float, n: int) -> float:
    ans = 1
    if n == 0:
        return 1
    if n == 1:
        return x
    elif n < 0:
        return myPow(1/x, n*-1)
    else:
        if n % 2 == 1:
            n -= 1
            ans *= x
        ans *= myPow(x*x, n/2)
    return ans
myPow(2, 10)
