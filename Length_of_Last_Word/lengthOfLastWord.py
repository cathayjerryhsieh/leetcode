"""
LeetCode
Title : 58. Length of Last Word
URL : https://leetcode.com/problems/length-of-last-word/
Difficulty : Easy
"""
import re

def lengthOfLastWord(s: str) -> int:
    return len(re.findall(r'(\w+)', s)[-1]) if re.findall(r'(\w+)', s) else 0

