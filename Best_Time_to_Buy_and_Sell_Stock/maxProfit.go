/**
LeetCode
Title : 121. Best Time to Buy and Sell Stock
URL : https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
Difficulty : Easy
**/

func maxProfit(prices []int) int {
	result := 0
	if len(prices) <= 0 {
		return 0
	}
	lastData := prices[0]
	tmp := 0
	for i := 1; len(prices) >= i; i++ {
		if lastData > prices[i-1] {
			lastData = prices[i-1]
			continue
		} else {
			tmp = prices[i-1] - lastData
			if result < tmp {
				result = tmp
			}
		}
	}
	return result
}