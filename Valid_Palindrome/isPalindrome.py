"""
LeetCode
Title : 125. Valid Palindrome
URL : https://leetcode.com/problems/valid-palindrome/
Difficulty : Easy
"""

import re

def isPalindrome(self, s):
    combin_text = ''.join(re.findall(r'[a-z0-9]+',s.lower()))
    reverse = combin_text[::-1]
    index = 0
    
    while int(len(combin_text)/2) > index :
        if combin_text[index] != reverse[index]:
            return False
        index += 1

    return True
