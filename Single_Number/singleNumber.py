"""
LeetCode
Title : 136. Single Number
URL : https://leetcode.com/problems/single-number/
Difficulty : Easy
"""

def singleNumber(nums:list) -> int:
    for i in range(1, len(nums)):
        nums[0] ^= nums[i]
    return nums[0]

