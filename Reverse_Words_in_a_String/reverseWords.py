"""
LeetCode
Title : 151. Reverse Words in a String
URL : https://leetcode.com/problems/reverse-words-in-a-string/
Difficulty : Medium
"""
import re

def reverseWords(s: str) -> str:
    return ' '.join(re.findall(r'\w+',s)[::-1])
